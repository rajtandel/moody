//
//  MoodyTests.swift
//  MoodyTests
//
//  Created by Raj ‎ on 05/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import XCTest
@testable import Moody

class MoodyTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testReadJson(){
        
        //Given
        if let path = Bundle.main.path(forResource: "moodymockdata", ofType: "json"){
            do {
                //When
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let  json = jsonResult as? Dictionary<String, AnyObject>{
                   //Then
                    XCTAssertNotNil(json)
                }
            }catch let error{
                   //Then
                XCTAssertNotNil(error.localizedDescription)
            }
        }
    }
    
    func testCheckForOptionTrue(){
        //Given
        var tempArray = [WSAnswer]()
        for _ in 0...3{
            let answer = WSAnswer(object: self)
            answer.isSelected = true
            tempArray.append(answer) //when
        }
        
        //Then
         let isOptionSelected = tempArray.filter({$0.isSelected == true})
        XCTAssertTrue(isOptionSelected.count > 0)
    
    }
    
    func testCheckForOptionFalse(){
        //Given
        var tempArray = [WSAnswer]()
        for _ in 0...3{
            let answer = WSAnswer(object: self)
            answer.isSelected = false
            tempArray.append(answer) //when
        }
        
        //Then
        let isOptionSelected = tempArray.filter({$0.isSelected == false})
        XCTAssertTrue(isOptionSelected.count > 0)
        
    }

    func testConfigDataModel(){
        
        //Given
        var tempAnsArray = [WSAnswer]()
        var dataModel = [WSAnswer] ()
        
        var arrSteps = [WSSteps]()
        for index in 1...4{
            let steps = WSSteps(object: self)
            steps.stepId = index
            arrSteps.append(steps) //when
        }
        
        XCTAssertTrue(arrSteps.count > 0) //Then
        
        //Given
        let stepObj = arrSteps[0]
        let arrQuestions = CDManager.sharedInstance.fetchQuestionsFromDB(stepId: stepObj.stepId!)
        for questions in arrQuestions{
            for answers in questions.answer!{
                tempAnsArray.append(answers) //When
            }
        }
        dataModel = tempAnsArray.sorted(by: {$0.answerId! < $1.answerId!})
        XCTAssertNotNil(dataModel) //Then
        
    }
}
