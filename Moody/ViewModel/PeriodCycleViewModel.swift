//
//  PeriodCycleViewModel.swift
//  Moody
//
//  Created by Raj ‎ on 07/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation

class PeriodCycleViewModel{
    
    var questions: String = ""
    var questionSummary: String = ""
    
    var dataModel:[WSAnswer]!{
        didSet{
            //add other stuff here, after property is set
        }
    }
    
    var pickerModel:[String]!{
        didSet{
            
        }
    }
    
    init() {
        configDataModel()
        
    }
    
    var noOfSections: Int{
        return 1
    }
    
    var noOfRows: Int{
        return self.dataModel.count
    }
    
     //MARK:----------------: Configuring Data Models
    func configDataModel(){
        
        var tempAnsArray = [WSAnswer]()
        if  let stepsObj = appDelegate.navStack?.arrSteps[(appDelegate.navStack?.currentIndex)!]{
            let arrQuestions = CDManager.sharedInstance.fetchQuestionsFromDB(stepId: stepsObj.stepId!)
            for questions in arrQuestions{
                self.questions = questions.question!
                self.questionSummary = questions.questionSummary!
                for answers in questions.answer!{
                    tempAnsArray.append(answers)
                }
            }
            self.dataModel = tempAnsArray
        }
        configPickerValues()
    }
    
     //MARK:----------------: Configuring Picker Values
    func configPickerValues(){
        var arr = [String]()
        for i in 1...30{
            let days = "\(i) \(StringConst.kDays)"
            arr.append(days)
        }
        self.pickerModel = arr
    }
}
