//
//  PeriodsViewModel.swift
//  Moody
//
//  Created by Raj ‎ on 06/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation
import UIKit


 //MARK:----------------: Options Protocols
protocol OptionsProtocol {
    //This protocol will be used in other view model classes.
    var noOfRows:Int{get}
    var noOfSections:Int{get}
    var questions:String{get set}
    var questionSummary:String{get set}
}


class PeriodsViewModel:OptionsProtocol{
    
    var questions: String = ""
    var questionSummary: String = ""
    
    //dataModel array variable is used to set the answers
    var dataModel:[WSAnswer]!{
        didSet{
            //add other stuff here, after property is set
        }
    }
    
    init() {
      configDataModel()
    }
    
    var noOfSections: Int{
        return 1
    }
    
    var noOfRows: Int{
        return self.dataModel.count
    }
    
     //MARK:----------------: Configuring Data Models
    func configDataModel(){
        
        //Adding all answers object in datamodel array.
        var tempAnsArray = [WSAnswer]()
        if  let stepsObj = appDelegate.navStack?.arrSteps[(appDelegate.navStack?.currentIndex)!]{
             let arrQuestions = CDManager.sharedInstance.fetchQuestionsFromDB(stepId: stepsObj.stepId!)
              for questions in arrQuestions{
                self.questions = questions.question!
                self.questionSummary = questions.questionSummary!
                for answers in questions.answer!{
                    tempAnsArray.append(answers)
                }
            }
            self.dataModel = tempAnsArray.sorted(by: {$0.answerId! < $1.answerId!})
        }
    }
}
