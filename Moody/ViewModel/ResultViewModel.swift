//
//  ResultViewModel.swift
//  Moody
//
//  Created by Raj ‎ on 06/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation
import UIKit

class  ResultViewModel: OptionsProtocol{

    var questions: String = ""
    var questionSummary: String = ""
   
    var dataModel:[WSAnswer]!{
        didSet{
            //add other stuff here, after property is set
        }
    }
    
    var noOfSections: Int{
        return 1
    }
    
    var noOfRows: Int{
        return self.dataModel.count
    }
    
    init() {
        self.configDataModel()
    }
    
     //MARK:----------------: Configuring Data Models
    func configDataModel(){
        
        var tempAnsArray = [WSAnswer]()
            if let arrSteps = appDelegate.navStack?.arrSteps{
                
                for steps in arrSteps{
                    
                    let arrQuestions = CDManager.sharedInstance.fetchQuestionsFromDB(stepId: steps.stepId!)
                    for questions in arrQuestions{
                        self.questions = questions.question!
                        self.questionSummary = questions.questionSummary!
                        for answers in questions.answer!{
                            if answers.isSelected!{
                                answers.answerSummary = "\(questions.questionResult ?? "")"
                                answers.answer = "- \(answers.answer ?? "")"
                                tempAnsArray.append(answers)
                            }
                        }
                    }
                }
                 self.dataModel = tempAnsArray
            }
    }
}
