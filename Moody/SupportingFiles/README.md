### Moody Month
    
    - Application mock made with the purpose of defining the major steps that helps users to keep track of their mood.

### Written in Swift 5.0
    - Xcode 10.2
    - CocoaPods is used for dependencies
    
### Coding Hierarchy
        - App follows MVVM (Model-View-View Model) architecture.

### Coding Hierarchy Breakdown:

    - Model: All model related classes are saved in this folder.
        - Constants - Contains application constants file.
        - ModelClasses - All the cell models and other model classes are under this folder.
        - CoreData - CoreData related files
        - Helpers - All helper files which contains some common codes used all over the application

    - View: Application views.
        - Storyboards - All Storyboards files are save under this folder
        - Controllers - Application controllers files

    - ViewModel - All the View model files which are related to controllers are saved under this folder.


    - Supporting Files - This folder contains all other files which helps to support the application structure. Like Info.Plist, CoreDataModel and Images.XCAssets.


### Database:  CoreData

    - To maintain the flexibility of application. Core Data functionality is used.
    - All core database related can be find under Model/CoreData folder.
    
### Test Cases (TDD):  Unit Testing
    
    - Some basic unit testing cases are written in "MoodyTest.Swift" file just to ensure the code runs on all the cases without crash.

### Note:
        - App use a mock json file name "moodymockdata.json" which is stored in SupportingFiles Folder.
        - All the info are stored in database from this json file.


