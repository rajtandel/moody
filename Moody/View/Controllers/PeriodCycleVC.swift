//
//  PeriodCycleVC.swift
//  Moody
//
//  Created by Raj ‎ on 06/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import UIKit

class PeriodCycleVC: UIViewController {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var pickerVwDays: UIPickerView!
    
    var viewModel:PeriodCycleViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        configViewModelClass()
        // Do any additional setup after loading the view.
    }
    
    func configUI(){
        setUpButtons(btnNext, text: StringConst.kDone)
        setUpButtons(btnBack, text: StringConst.kBack)
        setCancelBtn(btn: btnBack)
        setQuestionFont(lblQuestion)
    }
    
    func configViewModelClass(){
        viewModel = PeriodCycleViewModel()
        lblQuestion.text = viewModel?.questions
        lblMsg.text = viewModel?.questionSummary
        lblMsg.font = themeInstance.font_Campton_Bold(16)
        checkForOptionSelected(answers: (viewModel?.dataModel!)!, btn: btnNext)
    }
    
    //MARK:----------------: Button Actions
    @IBAction func btnBackAction(_ sender: Any) {
          appDelegate.navStack?.popToPreviousController(from:self.navigationController!)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
         //move to result VC
        appDelegate.navStack?.pushNextController(from: self.navigationController!)
        AppDefaults.setBoolValueToUserDefaults(boolValue: true, key: AppConst.DefaultKeys.kProcessCompleted)
    }
}

extension PeriodCycleVC : UIPickerViewDelegate, UIPickerViewDataSource{
     //MARK:----------------: PickerView DataSource & Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return  (viewModel?.pickerModel.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let currentSelectedObj = viewModel?.dataModel[0]{
            currentSelectedObj.isSelected = true
            currentSelectedObj.answer = viewModel?.pickerModel[row]
            CDManager.sharedInstance.updateAnswers(answer: currentSelectedObj)
             enableNext(enable: true, btn: btnNext)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: (viewModel?.pickerModel[row])!, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
    }
}
