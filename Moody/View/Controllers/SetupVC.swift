//
//  SetupVC.swift
//  Moody
//
//  Created by Raj ‎ on 05/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import UIKit

enum cellType:Int{
    case title = 0
    case body = 1
    case msg = 2
}

class SetupVC: UIViewController {

    @IBOutlet weak var imgVwLogo: UIImageView!
    @IBOutlet weak var btnSetup: UIButton!
    @IBOutlet weak var tblVwSetup: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        animateLogo()
    }
    
     //MARK:----------------: UI Setups
    func configUI(){
        btnSetup.layer.cornerRadius = 6
        btnSetup.setTitle(StringConst.kSetup, for: .normal)
        
        tblVwSetup.separatorColor = .clear
        tblVwSetup.tableFooterView = UIView()
        
        btnSetup.titleLabel?.font = themeInstance.font_Campton_SemiBold(12)
    }
    
    func animateLogo(){
        let originY = self.btnSetup.frame.origin.y
        self.imgVwLogo.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        self.btnSetup.frame = CGRect(x: self.btnSetup.frame.origin.x, y: self.view.frame.height * 2, width: self.btnSetup.frame.width, height: self.btnSetup.frame.height)
        UIView.animate(withDuration: 1.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.7, options: .curveEaseInOut, animations: {
            self.imgVwLogo.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.btnSetup.frame = CGRect(x: self.btnSetup.frame.origin.x, y: originY, width: self.btnSetup.frame.width, height: self.btnSetup.frame.height)
        }) { (completed) in
           
        }
    }
    
    //MARK:----------------: Button Actions
    @IBAction func btnSetupAction(_ sender: Any) {
        //reset all selected answer
        appDelegate.navStack?.resetAllSelctedAnswer()
        appDelegate.navStack?.pushNextController(from:self.navigationController!)
    }

}

extension SetupVC: UITableViewDelegate, UITableViewDataSource{
       //MARK:----------------:Tableview DataSource & Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.textLabel?.text = StringConst.kMoodyMonth
        cell?.textLabel?.textColor = .white
        cell?.textLabel?.textAlignment = .center
        cell?.textLabel?.numberOfLines = 0
        
        cell?.alpha  = 0
        
        UIView.animate(withDuration: 1, delay: 0.05 * Double(indexPath.row), options: .curveEaseInOut, animations: {
             cell?.alpha  = 1
        }, completion: nil)
        
        switch indexPath.row {
        case cellType.title.rawValue:
            cell?.textLabel?.text = StringConst.kMoodyMonth
            cell?.textLabel?.font = themeInstance.font_Campton_SemiBold(36)
        case cellType.body.rawValue:
            cell?.textLabel?.text = StringConst.kSetupSyncMsg
            cell?.textLabel?.font = themeInstance.font_Campton_Bold(16)
        case cellType.msg.rawValue:
            cell?.textLabel?.text = StringConst.kSetupSyncMsg2
            cell?.textLabel?.font = themeInstance.font_Campton_Bold(16)
        default: break

        }
        return cell!
    }
    
}
