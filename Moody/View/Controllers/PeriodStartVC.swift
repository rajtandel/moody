//
//  PeriodStartVC.swift
//  Moody
//
//  Created by Raj ‎ on 06/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import UIKit

class PeriodStartVC: UIViewController {

    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
     var viewModel:PeriodStartViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        configViewModelClass()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setDateAnswer()
    }
    
    //MARK:----------------: UI Setups
    func configUI(){

        setUpButtons(btnNext, text: StringConst.kNext)
        setUpButtons(btnBack, text: StringConst.kBack)
        setCancelBtn(btn: btnBack)
         setQuestionFont(lblQuestion)
       
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        datePicker.maximumDate = Date()
    }

    func configViewModelClass(){
        viewModel = PeriodStartViewModel()
        lblQuestion.text = viewModel?.questions
        checkForOptionSelected(answers: (viewModel?.dataModel!)!, btn: btnNext)
    }
    

  //MARK:----------------: Button Actions
    @IBAction func btnBackAction(_ sender: Any) {
          appDelegate.navStack?.popToPreviousController(from:self.navigationController!)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
       appDelegate.navStack?.pushNextController(from:self.navigationController!)
    }
    
    @IBAction func datePickerValueChangedAction(_ sender: Any) {
        enableNext(enable: true, btn: btnNext)
        setDateAnswer()
    }
    
     //MARK:----------------: Setting Dates
    func setDateAnswer(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, d MMMM yy"
        let stringFromDate = dateFormatter.string(from: datePicker.date)
        
        if let currentSelectedObj = viewModel?.dataModel[0]{
            currentSelectedObj.isSelected = true
            currentSelectedObj.answer = stringFromDate
            CDManager.sharedInstance.updateAnswers(answer: currentSelectedObj)
        }
    }
}
