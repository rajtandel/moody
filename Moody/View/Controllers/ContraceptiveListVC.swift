//
//  ContraceptiveListVC.swift
//  Moody
//
//  Created by Raj ‎ on 06/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import UIKit

class ContraceptiveListVC: UIViewController {

    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var tblVwList: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    var viewModel:ContraceptiveViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        configViewModelClass()
    }
    
    //MARK:----------------: UI Setups
    func configUI(){
       
        setTableFooters(tableview: tblVwList)
        setUpButtons(btnNext, text: StringConst.kNext)
        setUpButtons(btnBack, text: StringConst.kBack)
        setCancelBtn(btn: btnBack)
        setQuestionFont(lblQuestion)
    }
    
     func configViewModelClass(){
        viewModel = ContraceptiveViewModel()
        lblQuestion.text = viewModel?.questions
        checkForOptionSelected(answers: (viewModel?.dataModel!)!, btn: btnNext)
    }
    
     //MARK:----------------: Button Actions
    @IBAction func btnBackAction(_ sender: Any) {
        appDelegate.navStack?.popToPreviousController(from:self.navigationController!)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
      appDelegate.navStack?.pushNextController(from:self.navigationController!)
    }
}

extension ContraceptiveListVC: UITableViewDelegate, UITableViewDataSource{
     //MARK:----------------:Tableview DataSource & Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.noOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.noOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! OptionsTblCell
        cell.items = viewModel?.dataModel[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
 
        //check if any options is already selected. If yes then disable that option
        if let previousSelectedObj = viewModel?.dataModel.filter({$0.isSelected == true}).first {
            previousSelectedObj.isSelected = false
            CDManager.sharedInstance.updateAnswers(answer: previousSelectedObj)
        }
        
        //changing option of selected object.
        if let currentSelectedObj = viewModel?.dataModel[indexPath.row]{
            currentSelectedObj.isSelected = (currentSelectedObj.isSelected!) ? false : true
            CDManager.sharedInstance.updateAnswers(answer: currentSelectedObj)
        }
        
        enableNext(enable: true, btn: btnNext)
        tblVwList.reloadData()
    }
    
}

