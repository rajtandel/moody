//
//  ResultVC.swift
//  Moody
//
//  Created by Raj ‎ on 06/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import UIKit

class ResultVC: UIViewController {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var tblVwResult: UITableView!
    
     var viewModel:ResultViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       configViewModelClass()
    }
    
    //MARK:----------------: UI Setups
    func configUI(){
        
        setUpButtons(btnNext, text: StringConst.kLetsGo)
        setUpButtons(btnBack, text: StringConst.kGoBack)
        btnBack.layer.borderColor = UIColor.white.cgColor
        btnBack.layer.borderWidth = 1
        setTableFooters(tableview: tblVwResult)
        setQuestionFont(lblResult)
    }
    
    
    func configViewModelClass(){
        viewModel = nil
        viewModel = ResultViewModel()
        lblResult.text = viewModel?.questions
        tblVwResult.reloadData()
    }
    
     //MARK:----------------: Button Actions
    @IBAction func btnBackAction(_ sender: Any) {
        appDelegate.navStack?.popToStep2(from: self.navigationController!)
        AppDefaults.setBoolValueToUserDefaults(boolValue: false, key: AppConst.DefaultKeys.kProcessCompleted)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        AppDefaults.setBoolValueToUserDefaults(boolValue: true, key: AppConst.DefaultKeys.kMoveToHome)
        setRootViewController(sbName: AppConst.kSBHome, vcId: AppConst.kNavHomeVCId)
    }
}


extension ResultVC: UITableViewDelegate, UITableViewDataSource{
     //MARK:----------------:Tableview DataSource & Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
         return viewModel?.noOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return viewModel?.noOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ResultTblCell
        cell.alpha  = 0
        UIView.animate(withDuration: 1, delay: 0.05 * Double(indexPath.row), options: .curveEaseInOut, animations: {
            cell.alpha  = 1
        }, completion: nil)
        cell.items = viewModel?.dataModel[indexPath.row]
        
        return cell
    }
    
}
