//
//  SplashVC.swift
//  Moody
//
//  Created by Raj ‎ on 07/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configAppSetup()
    }
    
    //MARK:-------------------- App Setup funcitons
    func configAppSetup(){
        
        //Firstly check if all the steps are completed (i.e when user clicked on Done option). by checking boolean stored in user defaults
        let isStepsCompleted = AppDefaults.getBoolValueFromDefaults(key: AppConst.DefaultKeys.kProcessCompleted)
        if (isStepsCompleted){
            
            //check if user have completed all the steps and clicked on "Ok Lets Go option."
            if (isMoveToHomeVC()){
                //move to today screen
                setRootViewController(sbName: AppConst.kSBHome, vcId: AppConst.kNavHomeVCId)
            }else{
                //move to result screen
                fetchAllDataFromDB()
            }
        }else{
            //Read JSON from data and store it in database. Static json file kept in "Supporting Files" folder
            readJSON()
        }
      
    }
    
    func isMoveToHomeVC() -> Bool{
     return AppDefaults.getBoolValueFromDefaults(key: AppConst.DefaultKeys.kMoveToHome)
    }
    
      //MARK:-------------------- Parsing Json
    func readJSON(){
     
        if let path = Bundle.main.path(forResource: "moodymockdata", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let  json = jsonResult as? Dictionary<String, AnyObject>{
                       self.parseJson(json)
                }
            }catch let error{
                print("Error in reading Data: ",error.localizedDescription)
            }
        }else{
            print("Error in reading path of json file")
        }
    }
    
    func parseJson(_ json:Dictionary<String, AnyObject>){
       
        var tempStepObj = [WSSteps]()
        for (_ , value) in json{
            for values in (value as? Array<Any>)!{
                 let stepObj:WSSteps = WSSteps(object: values)
                tempStepObj.append(stepObj)
            }
        }
        deleteAllDataFromDB()
        saveJsonDataInDB(tempStepObj)
        fetchAllDataFromDB()
    }
    
    //MARK:-------------------- Database Functionality
    func saveJsonDataInDB(_ data:[WSSteps]){
        CDManager.sharedInstance.saveDataInDB(arrSteps: data) //saving data in Core Data Model
    }
    
    func deleteAllDataFromDB(){
        //deleting all data, first remove data from child and then remove it from parent table
        CDManager.sharedInstance.deleteData(pred: "", entity: CDConst.Tbl_Answers)
        CDManager.sharedInstance.deleteData(pred: "", entity: CDConst.Tbl_Questions)
        CDManager.sharedInstance.deleteData(pred: "", entity: CDConst.Tbl_Steps)
    }
    
    func fetchAllDataFromDB(){
        let  tmpDetails = CDManager.sharedInstance.fetchDetailsFromDB()
        let finalArr = tmpDetails.sorted(by: {$0.stepId! < $1.stepId!}) //sorting based on steps Id (Ascending Order)
        appDelegate.navStack = NavigationStack(arr: finalArr) //initialising navigation stack class object
    }

   
}
