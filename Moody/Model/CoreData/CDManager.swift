//
//  CDManager.swift
//  Moody
//
//  Created by Raj ‎ on 07/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation
import UIKit
import CoreData




/*
 
 - CDManager class is used to perform all the operations in our coreData
 
 */

let appDelegate = UIApplication.shared.delegate as! AppDelegate

class  CDManager {
    
    static let sharedInstance = CDManager()
    let syncMoc:NSManagedObjectContext! = appDelegate.dataAdaptor?.nsManagedObjectContext
    

    //MARK:----------------: Core Data Functions
    func fetchDataFromDB(entity:String, pred:String, viewContext: NSManagedObjectContext) -> Array<Any>{
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        if(pred != ""){
            request.predicate =  NSPredicate(format:pred)
        }
        request.returnsObjectsAsFaults = false
        do {
            let result = try viewContext.fetch(request)
            return result
        } catch {
            print("Failed")
        }
        return []
    }
    
    func fetchDataFromDBWithPred(entity:String, pred:NSPredicate?, viewContext: NSManagedObjectContext) -> Array<Any>{
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        if(pred != nil){
            request.predicate =  pred
        }
        request.returnsObjectsAsFaults = false
        do {
            let result = try viewContext.fetch(request)
            return result
        } catch {
            print("Failed")
        }
        return []
    }
    
   
   
    //MARK:----------------: Save  Details
    func saveDataInDB(arrSteps: [WSSteps]){
    
        for (_, steps) in arrSteps.enumerated(){
            
            //query for fetching all data
            var dbStepsObj = fetchDataFromDB(entity:CDConst.Tbl_Steps, pred: String(format:"stepId == %d",steps.stepId ?? 0), viewContext: syncMoc).first as? CD_Steps
        
            if dbStepsObj == nil{
                //check for nil and create new core data object
                dbStepsObj = NSEntityDescription.insertNewObject(forEntityName: CDConst.Tbl_Steps, into: syncMoc) as? CD_Steps
            }
            
            dbStepsObj?.stepId = Int16(steps.stepId!)
            dbStepsObj?.isLetsgo = false
            dbStepsObj?.controllerId = steps.controllerId
            saveQuestionInDB(steps,dbStepsObj!) //saving all questions
        }
        
        do {
            try  syncMoc.save()
        }catch let error as NSError {
            print("Error in saving  data : " , error.localizedDescription)
        }
        
    }
    
    func saveQuestionInDB(_ steps:WSSteps, _ dbStepsObj:CD_Steps){
        
        for (_ , question) in (steps.question?.enumerated())!{
            
            var dbQuestionObj = fetchDataFromDB(entity:CDConst.Tbl_Questions, pred: String(format:"stepId == %d",question.stepId ?? 0), viewContext: syncMoc).first as? CD_Questions
            if dbQuestionObj == nil{
                dbQuestionObj = NSEntityDescription.insertNewObject(forEntityName: CDConst.Tbl_Questions, into: syncMoc) as? CD_Questions
            }
            
            dbQuestionObj?.stepId = Int16(question.stepId!)
            dbQuestionObj?.questionId = Int16(question.questionId!)
            dbQuestionObj?.question = question.question
            dbQuestionObj?.questionResult = question.questionResult
            dbQuestionObj?.questionSummary = question.questionSummary
            dbStepsObj.addToSteps_question_relationship(dbQuestionObj!) //adding steps to question relationship
            
            //saving all answers
            saveAnswersInDB(question, dbQuestionObj!)
        
        }
    }
    
    func saveAnswersInDB(_ questions:WSQuestion, _ dbQuestionObj:CD_Questions){
        
        for (_ , answer) in (questions.answer?.enumerated())!{
            var dbAnswerObj = fetchDataFromDB(entity:CDConst.Tbl_Answers, pred: String(format:"answerId == %d",answer.answerId ?? 0), viewContext: syncMoc).first as? CD_Answers
            if dbAnswerObj == nil{
                dbAnswerObj = NSEntityDescription.insertNewObject(forEntityName: CDConst.Tbl_Answers, into: syncMoc) as? CD_Answers
            }
            dbAnswerObj?.answerId = Int16(answer.answerId ?? 0)
            dbAnswerObj?.answer  = answer.answer
            dbAnswerObj?.questionId  = Int16(answer.questionId ?? 0)
            dbAnswerObj?.isSelected = false
            dbQuestionObj.addToQuestion_answer_relationship(dbAnswerObj!) //adding question answer relationship
        }
    }
    
    
    //MARK:----------------: Fetch  Details from DB
    
    func fetchDetailsFromDB() -> [WSSteps]{
        
        var arrSteps = [WSSteps]()
        let arrDBSteps =  fetchDataFromDB(entity: CDConst.Tbl_Steps, pred:"",viewContext: syncMoc) as? [CD_Steps]
        
        if(arrDBSteps != nil && arrDBSteps?.count ?? 0  > 0){
            for(_ , dbStepsObj) in arrDBSteps!.enumerated(){
                let steps = WSSteps(object: self)
                steps.stepId =  Int(dbStepsObj.stepId)
                steps.controllerId = dbStepsObj.controllerId
                steps.isLetsgo = dbStepsObj.isLetsgo
                steps.question = fetchQuestionsFromDB(stepId: steps.stepId ?? 0)
                arrSteps.append(steps)
            }
            return arrSteps
        }
        return []
    }
    
    func fetchQuestionsFromDB(stepId:Int) -> [WSQuestion]{
        var arrQuestions = [WSQuestion]()
        let arrDBQuestions =  fetchDataFromDB(entity: CDConst.Tbl_Questions, pred:String(format:"stepId == %d",stepId),viewContext: syncMoc) as? [CD_Questions]
        
        if(arrDBQuestions != nil && arrDBQuestions?.count ?? 0  > 0){
            for(_ , dbQuestionObj) in arrDBQuestions!.enumerated(){
            
                let question = WSQuestion(object: self)
                question.stepId = Int(dbQuestionObj.stepId)
                question.questionId = Int(dbQuestionObj.questionId)
                question.question = dbQuestionObj.question
                question.questionResult = dbQuestionObj.questionResult
                question.questionSummary = dbQuestionObj.questionSummary
                question.answer = fetchAnswersFromDB(questionId: question.questionId ?? 0)
                arrQuestions.append(question)
            }
        }
        return arrQuestions
    }
    
    func fetchAnswersFromDB(questionId:Int) -> [WSAnswer]{
        
        var arrAnswers = [WSAnswer]()
        let arrDBAnswers =  fetchDataFromDB(entity: CDConst.Tbl_Answers, pred:String(format:"questionId == %d",questionId),viewContext: syncMoc) as? [CD_Answers]
        
        if(arrDBAnswers != nil && arrDBAnswers?.count ?? 0  > 0){
            for(_ , dbAnswerObj) in arrDBAnswers!.enumerated(){
                
                let answer = WSAnswer(object: self)
                answer.answerId = Int(dbAnswerObj.answerId)
                answer.answer = dbAnswerObj.answer
               answer.questionId = Int(dbAnswerObj.questionId)
                answer.isSelected = dbAnswerObj.isSelected
                arrAnswers.append(answer)
            }
        }
        return arrAnswers
    }
    
    
    func updateAnswers(answer:WSAnswer){
        
        let dbAnswerObj = fetchDataFromDB(entity:CDConst.Tbl_Answers, pred: String(format:"answerId == %d",answer.answerId ?? 0), viewContext: syncMoc).first as? CD_Answers
        dbAnswerObj?.isSelected = answer.isSelected!
        dbAnswerObj?.answer = answer.answer
        
        do {
            try  syncMoc.save()
        }catch let error as NSError {
            print("Error in saving  data : " , error.localizedDescription)
        }
    }
    
    func updateStepsCompleted(steps:WSSteps){
        
        let dbStepsObj = fetchDataFromDB(entity:CDConst.Tbl_Steps, pred: String(format:"stepId == %d",steps.stepId ?? 0), viewContext: syncMoc).first as? CD_Steps
        dbStepsObj?.isLetsgo = false
        do {
            try  syncMoc.save()
        }catch let error as NSError {
            print("Error in saving  data : " , error.localizedDescription)
        }
    }
    
    
    //MARK:----------------: Delete Data
    func deleteData(pred:String?, entity:String) {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        if(pred != ""){
            request.predicate = NSPredicate(format:pred!)
        }
        
        do {
            
            let result = try syncMoc.fetch(request)
            if(result.count > 0){
                for object in result{
                    guard let objectToDelete = object as? NSManagedObject else {continue}
                    syncMoc.delete(objectToDelete)
                }
            }
            do{
                try syncMoc.save()
            }catch let delError{
                print("Error in Deleting Data:  \(delError.localizedDescription)")
            }
            
        } catch let error {
            print("Error in Requesting Data:  \(error.localizedDescription)")
        }
    }
}

