//
//  CDConst.swift
//  Moody
//
//  Created by Raj ‎ on 07/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation


class CDConst {
    //storing all table name here
    static let Tbl_Steps = "CD_Steps"
    static let Tbl_Questions = "CD_Questions"
    static let Tbl_Answers = "CD_Answers"
}
