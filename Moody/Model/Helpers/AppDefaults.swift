//
//  AppDefaults.swift
//  Moody
//
//  Created by Raj ‎ on 06/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation


/*
 
 - AppDefaults class is used to perform all the operations on user defaults.
 - This class enchance the UserDefaults class and its functionality.
 
 */


class AppDefaults: UserDefaults {
    
    static  let userDefaults = UserDefaults.standard
    
    class func setStringValueToUserDefaults(str:String , key:String){
        userDefaults.set(str, forKey: key)
        userDefaults.synchronize()
    }
    
    class func getStringValueFromDefaults(key:String) -> String{
        guard let str = userDefaults.object(forKey: key) as? String else{
            return ""
        }
        return str
    }
    
    class func setBoolValueToUserDefaults(boolValue:Bool , key:String){
        userDefaults.set(boolValue, forKey: key)
        userDefaults.synchronize()
    }
    
    class func getBoolValueFromDefaults(key:String) -> Bool{
        let boolValue = userDefaults.object(forKey: key) as? Bool ?? false
        return boolValue
    }
}
