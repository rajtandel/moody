//
//  Helpers.swift
//  Moody
//
//  Created by Raj ‎ on 05/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation
import UIKit

//Helper Class
extension UIViewController {
    
    var themeInstance: Theme{
        return ThemeManager.currentTheme()
    }
    
    func loadVc(sbName:String, identifier:String) -> UIViewController{
        let sb = UIStoryboard.init(name: sbName, bundle: Bundle.main)
        let vc:UIViewController = sb.instantiateViewController(withIdentifier: identifier)
        return vc
    }
    
    
    func enableNext(enable:Bool, btn:UIButton){
        btn.alpha = enable ? 1 : 0.6
        btn.isEnabled = enable
    }
    
    func setUpButtons(_ btn:UIButton, text:String){
        btn.setTitle(text, for: .normal)
        btn.layer.cornerRadius = 6
        btn.titleLabel?.font = themeInstance.font_Campton_SemiBold(12)
    }
    
    func setCancelBtn(btn:UIButton){
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.borderWidth = 1
    }
    
    func setQuestionFont(_ label:UILabel){
        label.font = themeInstance.font_Campton_Bold(20)
    }
    
    func setTableFooters(tableview:UITableView){
        tableview.separatorColor = .clear
        tableview.tableFooterView = UIView()
    }
    
    func setRootViewController(sbName:String, vcId:String){
        let rootViewController = loadVc(sbName:sbName, identifier: vcId) as? UINavigationController
        appDelegate.window?.rootViewController = nil //setting nil as we have already setup splash vc to rootview controller
        appDelegate.window?.rootViewController = rootViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func checkForOptionSelected(answers:[WSAnswer], btn:UIButton){
        let isOptionSelected = answers.filter({$0.isSelected == true}) //if any option is selected then enable the next button
        if (isOptionSelected.count > 0){
            enableNext(enable: true, btn: btn)
        }else{
            enableNext(enable: false, btn: btn)
        }
    }
}
