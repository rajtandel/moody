//
//  NavigationStack.swift
//  Moody
//
//  Created by Raj ‎ on 07/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation
import UIKit

/*
 
 - Helper Class to configure the navigation stack of the controllers.
 - This class enchance the UserDefaults class and its functionality.
 
 */

class NavigationStack{
    
    public var currentController:UIViewController?
    private var arrViewControllers = [UIViewController]()
    public var currentIndex = -1 //Setting -1, as current Index of array starts from 0
    private var rootViewController:UINavigationController?
    public var arrSteps = [WSSteps]()
    var  isProcessCompleted = AppDefaults.getBoolValueFromDefaults(key: AppConst.DefaultKeys.kProcessCompleted)
    
     //MARK:-------------------- Initialization
    init(arr:[WSSteps]) {
        self.arrSteps = arr
        configNavStack(arr)
    }
    
  private  func configNavStack(_ arr:[WSSteps]){
    
        configArrayOfVC(arr)
        if arrViewControllers.count > 0{
            setUpRootController()
        }else{
            print("Error in configuring navigation stack array.")
        }
    }
    
    private func configArrayOfVC(_ arr:[WSSteps]){
        for stacks in arr{
            //adding all view controller in array.
            let vc = UIViewController().loadVc(sbName: AppConst.kSBMain, identifier: stacks.controllerId!)
            arrViewControllers.append(vc)
        }
    }
    
   private func setUpRootController(){
    
        rootViewController = UIViewController().loadVc(sbName: AppConst.kSBMain, identifier: AppConst.kRootNavigationVCId) as? UINavigationController
        appDelegate.window?.rootViewController = nil //setting nil as we have already setup splash vc to rootview controller
        appDelegate.window?.rootViewController = rootViewController
        appDelegate.window?.makeKeyAndVisible()
    
        if isProcessCompleted{
            //checking this condition when user have completed all the steps. So we show user direct the Summary screen.
            moveToResultVC()
        }
    }

    public func resetAllSelctedAnswer(){
        for stacks in self.arrSteps{
            for  questions in stacks.question!{
                for answer in questions.answer!{
                    answer.isSelected = false
                    CDManager.sharedInstance.updateAnswers(answer: answer)
                }
            }
        }
    }
    
     //MARK:-------------------- Navigating Controller Functions
    
    public  func pushNextController(from controller:UINavigationController){
        currentIndex += 1
        loadViewControllerFromArray(currentIndex, controller, animated: true)
    }
    
    public  func popToPreviousController(from controller:UINavigationController){

        if currentIndex > 0{
            currentIndex -= 1
           currentController = arrViewControllers[currentIndex]
        }else{
            resetIndex()
        }
        controller.popViewController(animated: true)
    }
    
    public  func popToStep2(from controller:UINavigationController){
    
        //This function is getting called when user Press "Go Back and Edit" from Result Screen
        
        if isProcessCompleted && currentController == nil{
            //checking this condtion for the scenario when user have completed the process but restarted the app.
            currentIndex = 0
            currentController = arrViewControllers[currentIndex]
            controller.popViewController(animated: false)
            loadViewControllerFromArray(currentIndex, controller, animated: false)
        }else{
            currentIndex = 0
            currentController = arrViewControllers[currentIndex]
            controller.popToViewController(currentController!, animated: true)
        }
    }
    
    public func resetIndex(){
            currentIndex = -1
            arrViewControllers.removeAll()
            configArrayOfVC(self.arrSteps) //adding all the objects of view controller again in array after resetting.
    }
    
    private  func loadViewControllerFromArray(_ index:Int, _ controller:UINavigationController,animated:Bool){
        controller.pushViewController(arrViewControllers[index], animated: animated)
        currentController = arrViewControllers[index]
    }
    
    private func moveToResultVC(){
        let resultVc = UIViewController().loadVc(sbName: AppConst.kSBMain, identifier: AppConst.kResultVCId)
        rootViewController?.pushViewController(resultVc, animated: false)
    }
}
