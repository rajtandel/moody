//
//  ThemeManager.swift
//  Moody
//
//  Created by Raj ‎ on 05/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation
import UIKit


//MARK:----------------:  UIColor Extension

let theme1Hex:String = "56cdc0" //RGB: 86 205 192 #aqua Marin
let theme2Hex:String = "ffffff"

extension UIColor{
    func colorFromHexString(hexString: String) -> UIColor{
        
        var cString:String  = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if(cString.hasPrefix("#")){
            cString.remove(at: cString.startIndex)
        }
        
        if(cString.count != 6){
            return UIColor.black //default Color
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func rgbWithAlpha(r:CGFloat, g:CGFloat, b:CGFloat , alpha:CGFloat) -> UIColor{
        return UIColor.init(red: r/255, green: g/255, blue: b/255, alpha: alpha)
    }
}


//MARK:----------------: Themes Enumeration
enum Theme:Int {
    
    case defaultTheme1, defaultTheme2
    
    var backgroundColor: UIColor {
        switch self {
        case .defaultTheme1:
            return UIColor().colorFromHexString(hexString:theme1Hex)
        case .defaultTheme2:
            return UIColor().colorFromHexString(hexString:theme2Hex)
        }
    }
    
    var primaryColor: UIColor {
        switch self {
        case .defaultTheme1:
            return UIColor().colorFromHexString(hexString:theme1Hex)
        case .defaultTheme2:
            return UIColor().colorFromHexString(hexString:theme2Hex)
        }
    }
    
    var whiteColor: UIColor {
        switch self {
        case .defaultTheme1:
            return UIColor().colorFromHexString(hexString:"ffffff")
        case .defaultTheme2:
            return UIColor().colorFromHexString(hexString:"ffffff")
        }
    }
    
    func font_System_Regular(_ size: CGFloat) -> UIFont{
        return UIFont.systemFont(ofSize: size)
    }
    
    func font_System_Bold(_ size: CGFloat) -> UIFont{
        return UIFont.boldSystemFont(ofSize: size)
    }
    
    func font_System_SemiBold(_ size: CGFloat) -> UIFont{
        return UIFont.init(name: "System-Semibold", size: size)!
    }
    
    func font_System_Medium(_ size: CGFloat) -> UIFont{
        return UIFont.systemFont(ofSize: size, weight: .medium)
    }
    
    func font_System_Heavy(_ size: CGFloat) -> UIFont{
        return UIFont.systemFont(ofSize: size, weight: .heavy)
    }
    
    func font_Campton_SemiBold(_ size: CGFloat) -> UIFont{
        return UIFont(name: "campton-semibold", size: size)!
    }
    
    func font_Campton_Bold(_ size: CGFloat) -> UIFont{
        return UIFont(name: "Campton-BoldDEMO", size: size)!
    }
    
    func font_Campton_Medium(_ size: CGFloat) -> UIFont{
        return UIFont(name: "campton-medium", size: size)!
    }
    
    func font_Campton_Book(_ size: CGFloat) -> UIFont{
        return UIFont(name: "campton-book", size: size)!
    }

    
}



let SelectedThemeKey = "SelectedTheme"

//MARK:----------------: ThemeManger Class Allocation
class ThemeManager {
    
    static   func currentTheme() -> Theme{
        if let storedTheme = (UserDefaults.standard.value(forKey: SelectedThemeKey) as AnyObject).integerValue {
            return Theme(rawValue: storedTheme)!
        } else {
            return .defaultTheme2
        }
    }
    
    static  func applyTheme(theme: Theme) {
        
        // First persist the selected theme using NSUserDefaults.
        
        UserDefaults.standard.setValue(theme.rawValue, forKey: SelectedThemeKey)
        UserDefaults.standard.synchronize()
        
        
        // You get your current (selected) theme and apply the main color to the tintColor property of your application’s window.
        let sharedApplication = UIApplication.shared
        sharedApplication.delegate?.window??.tintColor = theme.primaryColor
        
    
    }
}
