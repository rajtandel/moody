//
//  AppConst.swift
//  Moody
//
//  Created by Raj ‎ on 05/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation
import UIKit


struct AppConst {
    
    static let appContext:AppDelegate = UIApplication.shared.delegate as! AppDelegate
  
    
    //MARK:----------------: VC Identifiers
    static let kSBMain = "Main"
    static let kSBSplash = "Splash"
    static let kSBHome = "Home"
    
    static let kSplashVCId = "idSplashVC"
    static let kRootNavigationVCId = "idRootNavigationVC"
    static let kNavHomeVCId = "idNavHomeVC"
    
    static let kSetupVCId = "idSetupVC"
    static let kPeriodVCId = "idPeriodsVC"
    static let kContraceptiveVCId = "idContraceptiveListVC"
    static let kPeriodStartVCId = "idPeriodStartVC"
    static let kPeriodCycleVCId = "idPeriodCycleVC"
    static let kResultVCId = "idResultVC"
    
    
    struct DefaultKeys {
        static let kdataModelObject = "DATA_MODEL_OBJECT"
        static let kMoveToHome = "MOVE_TO_HOME"
        static let kProcessCompleted = "PROCESS_COMPLETED"
    }
}
