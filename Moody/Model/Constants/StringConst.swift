//
//  StringConst.swift
//  Moody
//
//  Created by Raj ‎ on 05/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import Foundation


struct StringConst {
    
    static let kAppName = NSLocalizedString("LK_AppName", comment: "")
    static let kMoodyMonth = NSLocalizedString("LK_MoodyMonth", comment: "")
    static let kSetup = NSLocalizedString("LK_SetUp", comment: "")
    static let kSetupSyncMsg = NSLocalizedString("LK_SyncUpMsg", comment: "")
    static let kSetupSyncMsg2 = NSLocalizedString("LK_SyncUpMsg2", comment: "")
    static let kNext = NSLocalizedString("LK_Next", comment: "")
    static let kCancel = NSLocalizedString("LK_Cancel", comment: "")
    static let kBack = NSLocalizedString("LK_Back", comment: "")
    static let kDone = NSLocalizedString("LK_Done", comment: "")
    static let kGoBack = NSLocalizedString("LK_GoBack", comment: "")
    static let kLetsGo = NSLocalizedString("LK_LetsGo", comment: "")
    static let kDays = NSLocalizedString("LK_Days", comment: "")

}
