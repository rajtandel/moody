//
//  WSAnswer.swift
//
//  Created by Raj ‎ on 07/07/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class WSAnswer: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let answer = "answer"
    static let isSelected = "isSelected"
    static let answerId = "answerId"
    static let questionId = "questionId"
     static let answerSummary = "answerSummary"
    
    
  }

  // MARK: Properties
  public var answer: String?
  public var isSelected: Bool? = false
  public var answerId: Int?
  public var questionId: Int?
 public var answerSummary: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    answer = json[SerializationKeys.answer].string
    isSelected = json[SerializationKeys.isSelected].boolValue
    answerId = json[SerializationKeys.answerId].int
    questionId = json[SerializationKeys.questionId].int
    answerSummary = json[SerializationKeys.answerSummary].string
    
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = answer { dictionary[SerializationKeys.answer] = value }
    dictionary[SerializationKeys.isSelected] = isSelected
    if let value = answerId { dictionary[SerializationKeys.answerId] = value }
    if let value = questionId { dictionary[SerializationKeys.questionId] = value }
    if let value = answerSummary { dictionary[SerializationKeys.answerSummary] = value }
    
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.answer = aDecoder.decodeObject(forKey: SerializationKeys.answer) as? String
    self.isSelected = aDecoder.decodeBool(forKey: SerializationKeys.isSelected)
    self.answerId = aDecoder.decodeObject(forKey: SerializationKeys.answerId) as? Int
    self.questionId = aDecoder.decodeObject(forKey: SerializationKeys.questionId) as? Int
     self.answerSummary = aDecoder.decodeObject(forKey: SerializationKeys.answerSummary) as? String
    
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(answer, forKey: SerializationKeys.answer)
    aCoder.encode(isSelected, forKey: SerializationKeys.isSelected)
    aCoder.encode(answerId, forKey: SerializationKeys.answerId)
    aCoder.encode(questionId, forKey: SerializationKeys.questionId)
     aCoder.encode(answerSummary, forKey: SerializationKeys.answerSummary)
    
  }

}
