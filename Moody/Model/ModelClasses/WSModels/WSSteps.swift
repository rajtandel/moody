//
//  WSSteps.swift
//
//  Created by Raj ‎ on 07/07/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class WSSteps: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let stepId = "stepId"
    static let question = "question"
    static let controllerId = "controllerId"
    static let isLetsgo = "isLetsgo"
    
    
  }

  // MARK: Properties
  public var stepId: Int?
    public var controllerId: String?
      public var isLetsgo: Bool?
    
  public var question: [WSQuestion]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    stepId = json[SerializationKeys.stepId].int
    isLetsgo = json[SerializationKeys.isLetsgo].bool
    controllerId = json[SerializationKeys.controllerId].string
    if let items = json[SerializationKeys.question].array { question = items.map { WSQuestion(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = stepId { dictionary[SerializationKeys.stepId] = value }
    if let value = controllerId { dictionary[SerializationKeys.controllerId] = value }
    if let value = isLetsgo { dictionary[SerializationKeys.isLetsgo] = value }
    
    if let value = question { dictionary[SerializationKeys.question] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    
    self.stepId = aDecoder.decodeObject(forKey: SerializationKeys.stepId) as? Int
        self.isLetsgo = aDecoder.decodeObject(forKey: SerializationKeys.isLetsgo) as? Bool
     self.controllerId = aDecoder.decodeObject(forKey: SerializationKeys.controllerId) as? String
    self.question = aDecoder.decodeObject(forKey: SerializationKeys.question) as? [WSQuestion]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(stepId, forKey: SerializationKeys.stepId)
    aCoder.encode(controllerId, forKey: SerializationKeys.controllerId)
    aCoder.encode(question, forKey: SerializationKeys.question)
    aCoder.encode(isLetsgo, forKey: SerializationKeys.isLetsgo)
  }

}
