//
//  WSQuestion.swift
//
//  Created by Raj ‎ on 07/07/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class WSQuestion: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let answer = "answer"
    static let stepId = "stepId"
    static let questionResult = "questionResult"
    static let questionSummary = "questionSummary"
    static let questionId = "questionId"
    static let question = "question"
  }

  // MARK: Properties
  public var answer: [WSAnswer]?
  public var stepId: Int?
  public var questionResult: String?
  public var questionSummary: String?
  public var questionId: Int?
  public var question: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.answer].array { answer = items.map { WSAnswer(json: $0) } }
    stepId = json[SerializationKeys.stepId].int
    questionResult = json[SerializationKeys.questionResult].string
    questionSummary = json[SerializationKeys.questionSummary].string
    questionId = json[SerializationKeys.questionId].int
    question = json[SerializationKeys.question].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = answer { dictionary[SerializationKeys.answer] = value.map { $0.dictionaryRepresentation() } }
    if let value = stepId { dictionary[SerializationKeys.stepId] = value }
    if let value = questionResult { dictionary[SerializationKeys.questionResult] = value }
    if let value = questionSummary { dictionary[SerializationKeys.questionSummary] = value }
    if let value = questionId { dictionary[SerializationKeys.questionId] = value }
    if let value = question { dictionary[SerializationKeys.question] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.answer = aDecoder.decodeObject(forKey: SerializationKeys.answer) as? [WSAnswer]
    self.stepId = aDecoder.decodeObject(forKey: SerializationKeys.stepId) as? Int
    self.questionResult = aDecoder.decodeObject(forKey: SerializationKeys.questionResult) as? String
    self.questionSummary = aDecoder.decodeObject(forKey: SerializationKeys.questionSummary) as? String
    self.questionId = aDecoder.decodeObject(forKey: SerializationKeys.questionId) as? Int
    self.question = aDecoder.decodeObject(forKey: SerializationKeys.question) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(answer, forKey: SerializationKeys.answer)
    aCoder.encode(stepId, forKey: SerializationKeys.stepId)
    aCoder.encode(questionResult, forKey: SerializationKeys.questionResult)
    aCoder.encode(questionSummary, forKey: SerializationKeys.questionSummary)
    aCoder.encode(questionId, forKey: SerializationKeys.questionId)
    aCoder.encode(question, forKey: SerializationKeys.question)
  }

}
