//
//  PeriodsTblCell.swift
//  Moody
//
//  Created by Raj ‎ on 05/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import UIKit

class OptionsTblCell: UITableViewCell {

    @IBOutlet weak var btnAnswers: UIButton!
    @IBOutlet weak var lblAnswers: UILabel!
    @IBOutlet weak var btnOptions: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    var items:WSAnswer?{
        didSet{
            guard let items = items else{
                return
            }
            self.lblAnswers.numberOfLines = 2
            self.lblAnswers.text = items.answer
            self.btnAnswers.setImage(UIImage(named: (items.isSelected! ? "icSelected" : "icUnselected")), for: .normal)
            self.lblAnswers.font = items.isSelected! ? ThemeManager.currentTheme().font_Campton_Bold(16) : ThemeManager.currentTheme().font_Campton_Medium(16)
        }
    }
}
