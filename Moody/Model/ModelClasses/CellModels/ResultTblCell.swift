//
//  ResultTblCell.swift
//  Moody
//
//  Created by Raj ‎ on 06/07/2019.
//  Copyright © 2019 Raj ‎. All rights reserved.
//

import UIKit

class ResultTblCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    var items:WSAnswer?{
        didSet{
            guard let items = items else{
                return
            }
            self.textLabel?.text = items.answerSummary
             self.textLabel?.font = ThemeManager.currentTheme().font_Campton_Bold(16)
            self.detailTextLabel?.text = items.answer
            self.detailTextLabel?.font = ThemeManager.currentTheme().font_Campton_Book(16)
            
        }
    }
}
